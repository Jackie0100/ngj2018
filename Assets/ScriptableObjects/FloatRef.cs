﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/FloatRef")]
public class FloatRef : ScriptableObject
{
    [SerializeField]
    private float value;

    public float Value
    {
        get
        {
            return value;
        }

        set
        {
            this.value = value;
        }
    }
}
