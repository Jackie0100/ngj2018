﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/Attack")]
public class Attack : ScriptableObject
{
    [SerializeField]
    string _name;
    [SerializeField]
    [TextArea]
    string _message;
    [SerializeField]
    GameEvent _event;

    public string Message
    {
        get
        {
            return _message;
        }

        set
        {
            _message = value;
        }
    }

    public string Name
    {
        get
        {
            return _name;
        }

        set
        {
            _name = value;
        }
    }

    public void DoAttack()
    {
        _event.Invoke();
    }

    public Vector2 AttackLocation()
    {
        return new Vector2(Random.Range(0, 12), Random.Range(0, 4));
    }

    public float GetAngle(int x)
    {
        return 360 - ((x * 45) + 22.5f);
    }


}
