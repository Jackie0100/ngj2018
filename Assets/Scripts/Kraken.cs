﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Kraken : MonoBehaviour
{
    [SerializeField]
    List<Transform> _tentacles;
    [SerializeField]
    KrakenAttackAnimationEventConnector[] _animEventCon;
    [Space(20)]
    [SerializeField]
    Animator _animatorKraken;
    [SerializeField]
    Animator _animatorWaves;

    [SerializeField]
    private int index;

    [HideInInspector]
    private int health = 5;

    [SerializeField]
    GameObject _gameOverScreen;

    [SerializeField]
    GameObject _buttonContinue;

    [SerializeField]
    Text _topText;

    [SerializeField]
    Text _botText;



    public List<Transform> Tentacles
    {
        get
        {
            return _tentacles;
        }

        set
        {
            _tentacles = value;
        }
    }

    public int Health
    {
        get
        {
            return health;
        }

        set
        {
            health = value;
            if (health == 0)
            {
                _gameOverScreen.SetActive(true);
            }
        }
    }

    void Start()
    {
        //DoAttack();
    }

    bool continueAttack = true;
    bool spitturn = true;

    public void StartAttacking()
    {
        StartCoroutine(AttackSequence());
    }

    IEnumerator AttackSequence()
    {
        continueAttack = false;

        if (spitturn)
        {
            index = 3;
            _animEventCon[3].Attack.DoAttack();
            _animatorKraken.SetBool(_animEventCon[3].AnimationEvent, true);
            _animatorWaves.SetBool(_animEventCon[3].AnimationEvent, true);
            spitturn = false;
        }
        else
        {
            index = 4;
            _animEventCon[4].Attack.DoAttack();
            _animatorKraken.SetBool(_animEventCon[4].AnimationEvent, true);
            _animatorWaves.SetBool(_animEventCon[4].AnimationEvent, true);
            spitturn = true;
        }

        _buttonContinue.SetActive(true);

        while (!continueAttack)
        {
            yield return new WaitForEndOfFrame();
        }

        continueAttack = false;
        DoAttack();

        _buttonContinue.SetActive(true);


        while (!continueAttack)
        {
            yield return new WaitForEndOfFrame();
        }

        continueAttack = false;
        DoAttack();

        _buttonContinue.SetActive(true);


        while (!continueAttack)
        {
            yield return new WaitForEndOfFrame();
        }

    }

    public void ContinueAttack()
    {
        continueAttack = true;
    }

    public void ResetGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void DoAttack()
    {
        _animatorKraken.SetBool(_animEventCon[index].AnimationEvent, false);
        _animatorWaves.SetBool(_animEventCon[index].AnimationEvent, false);
        index = Random.Range(0, _animEventCon.Length);
        if (index >= 0 || index < _animEventCon.Length)
        {
            _animEventCon[index].Attack.DoAttack();
        }
    }

    public void DealDamage(int dam)
    {
        Health -= dam;
    }

    public void Frail()
    {
#if UNITY_EDITOR
        index = 0;
#endif
        Debug.Log("Frail");
        SetAnimationState();
        int loc = (Random.Range(0, 8) + 1);
        int wise = Random.Range(0, 2);
        _botText.text = string.Format(_animEventCon[index].Attack.Message, loc.ToString(), ((wise == 0) ? (loc + 1).ToString() : (loc + 1).ToString()), ((wise == 0) ? "Clock" : "AntiClock"));
        _topText.text = string.Format(_animEventCon[index].Attack.Message, loc.ToString(), ((wise == 0) ? (loc + 1).ToString() : (loc + 1).ToString()), ((wise == 0) ? "Clock" : "AntiClock"));
    }

    public void Lash()
    {
#if UNITY_EDITOR
        index = 1;
#endif
        Debug.Log("Lash");
        SetAnimationState();
        int loc1 = (Random.Range(0, 8) + 1);
        int loc2 = (Random.Range(0, 8) + 1);

        while (loc1 == loc2)
        {
            loc2 = (Random.Range(0, 8) + 1);
        }

        _topText.text = string.Format(_animEventCon[index].Attack.Message, loc1.ToString(), loc2.ToString());
        _botText.text = string.Format(_animEventCon[index].Attack.Message, loc1.ToString(), loc2.ToString());
    }

    public void Spit()
    {
#if UNITY_EDITOR
        index = 3;
#endif
        Debug.Log("Spit");
        SetAnimationState();
        int loc1 = (Random.Range(0, 8) + 1);
        int loc2 = (Random.Range(0, 8) + 1);

        while (loc1 == loc2)
        {
            loc2 = (Random.Range(0, 8) + 1);
        }

        _topText.text = string.Format(_animEventCon[index].Attack.Message, loc1.ToString(), loc2.ToString());
        _botText.text = string.Format(_animEventCon[index].Attack.Message, loc1.ToString(), loc2.ToString());
    }

    public void Suck()
    {
#if UNITY_EDITOR
        index = 4;
#endif
        Debug.Log("Suck");
        SetAnimationState();
        _topText.text = _animEventCon[index].Attack.Message;
        _botText.text = _animEventCon[index].Attack.Message;
    }

    public void Thrash()
    {
#if UNITY_EDITOR
        index = 2;
#endif
        Debug.Log("Thrash");
        SetAnimationState();
        _topText.text = _animEventCon[index].Attack.Message;
        _botText.text = _animEventCon[index].Attack.Message;
    }

    public void SetAnimationState()
    {
        _animatorKraken.SetBool(_animEventCon[index].AnimationEvent, false);
        _animatorWaves.SetBool(_animEventCon[index].AnimationEvent, false);
        _animatorKraken.SetBool(_animEventCon[index].AnimationEvent, true);
        _animatorWaves.SetBool(_animEventCon[index].AnimationEvent, true);
    }
}
