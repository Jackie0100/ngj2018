﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class KrakenAttackAnimationEventConnector
{
    [SerializeField]
    Attack _attack;
    [SerializeField]
    GameEventListener _eventListener;
    [SerializeField]
    Animation _anim;
    [SerializeField]
    string _animationEvent;

    public Attack Attack
    {
        get
        {
            return _attack;
        }

        set
        {
            _attack = value;
        }
    }

    public GameEventListener EventListener
    {
        get
        {
            return _eventListener;
        }

        set
        {
            _eventListener = value;
        }
    }

    public Animation Anim
    {
        get
        {
            return _anim;
        }

        set
        {
            _anim = value;
        }
    }

    public string AnimationEvent
    {
        get
        {
            return _animationEvent;
        }

        set
        {
            _animationEvent = value;
        }
    }
}